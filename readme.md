# joyous

## Prerequisites

Setup your environment by following the steps for React Native CLI in https://reactnative.dev/docs/environment-setup

## Getting started

1. Clone this project source code
2. Navigate to the project root: `$ cd joyous`
3. Run `$ npm install`, then `$ cd ios && pod install`
4. Return to root folder and run `$ npx react-native run-ios` 

You should see the app running in the iOS Simulator shortly


import React, {useEffect, useState, createContext} from 'react';
import { Provider } from 'react-redux';
import {
  Platform,
  StyleSheet,
  View,
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { PersistGate } from 'redux-persist/integration/react'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import * as COLOR from './src/colour';
import storage from './src/store';

import Home from './src/module/home';
import Login from './src/module/login';
import Register from './src/module/register';
import Profile from './src/module/profile';

const Stack = createNativeStackNavigator()
const Tab = createBottomTabNavigator()

const Store = storage().store
const Persistor = storage().persistor

const App = () => {
  const [isLogin, setLogin] = useState(false)

  useEffect(() => {
    checkLogin()

    const unsubscribe = Store.subscribe(()=>{
      checkLogin()
    })

    return unsubscribe;
  }, [])

  const checkLogin = () => {
    console.log(Store.getState());
    if(Store.getState().currentUser) setLogin(true)
    else setLogin(false)
  }

  const HomeStack = ({route}) =>{
    return(
      <Tab.Navigator
        screenOptions={({ route }) => ({
          headerShown: false,
          tabBarShowLabel: false,
          tabBarIcon: ({ focused }) => {
            let iconName;
            let iconSize = 30;

            if (route.name === 'Home') {
              iconName = 'home-outline'
            } 
            else if (route.name === 'Profile') {
              iconName = 'account-outline'
            }

            return (
              <Icon name={iconName} size={iconSize} style={{color:focused?COLOR.BLACK_HEX:COLOR.GREY_999_HEX}}/>
            )
          }
        })}
      >
        <Tab.Screen name='Home' component={Home} />
        <Tab.Screen name='Profile' component={Profile} />
      </Tab.Navigator>
    )
  }

  return (
    <Provider store={Store}>
       <PersistGate loading={null} persistor={Persistor}>
        <NavigationContainer>    
          <Stack.Navigator 
            initialRouteName={isLogin?'Landing':'Login'}
            screenOptions={{
              header: () => {        
                return (
                  <View style={{height:Platform.OS === 'ios' ? '8%': '6%', backgroundColor:COLOR.WHITE_HEX}}/>
                );
              }
            }}
          >
            {isLogin ?
              <Stack.Screen name='Landing' component={HomeStack}/>:
              <>
                <Stack.Screen name='Login' component={Login} />
                <Stack.Screen name='Register' component={Register} />
              </>
            }
          </Stack.Navigator>
        </NavigationContainer>
      </PersistGate>
    </Provider>
  );
};

const styles = StyleSheet.create({
});

export default App;

let lastid=0

const initialState={
  currentUser: null,
  userList: [],
  whiteboardList: []
}

export default reducer = (state = initialState, action) =>{
  switch(action.type){
    case 'userUpdated':
      return {
        ...state,
        currentUser: action.payload.data
      }
    case 'userAdded':
      return {
        ...state,
        userList: [...state.userList, action.payload.data]
      }
    case 'userReset':
      return {
        ...state,
        userList: []
      }
    case 'whiteboardAdded':
      return {
        ...state,
        whiteboardList: [...state.whiteboardList, action.payload.data]
      }
    case 'whiteboardUpdated':
      return {
        ...state,
        whiteboardList: state.whiteboardList.map(item=> item.id !== action.payload.data.id ? item : 
          { ...item, title: action.payload.data.title, description: action.payload.data.description})
      }
    case 'whiteboardDeleted':
      return {
        ...state,
        whiteboardList: state.whiteboardList.filter(item=> item.id !== action.payload.id)
      }
    default:
      return state
  } 
}

// {
//   bugs:[
//     {
//       id:1,
//       description:'',
//       resolved: false
//     }
//   ],
//   currentUser:{}
// }
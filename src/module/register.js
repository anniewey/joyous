import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  ActivityIndicator,
  Alert,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import * as COLOR from '../colour';

const IMG_LOGO = require('../../assets/image/logo.png')

const Register = ({ navigation }) => {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [isPasswordVisible, setPasswordVisible] = useState(false)
  const [isConfirmPasswordVisible, setConfirmPasswordVisible] = useState(false)
  const [isLoading, setLoading] = useState(false)

  const dispatch = useDispatch()
  const userListData = useSelector(state=> state.userList)

  const onPressReturn = () => {
    navigation.goBack()
  }

  const onPressRegister = () => {

    console.log(userListData);

    if(!username || !password || !confirmPassword){
      Alert.alert('Error', 'Please fill in all the empty fields.')
      return
    }
    else if(password !== confirmPassword){
      Alert.alert('Error', 'Your password and confirmation password do not match.')
      return
    }
    else if(userListData.find(item=>item.username===username)){
      Alert.alert('Error', 'This username has been taken. Please try again.')
      return
    }

    setLoading(true)

    dispatch({
      type: 'userAdded',
      payload: {
        data: { id: userListData.length + 1 ,username, password }
      }
    })

    setLoading(false)

    Alert.alert(
      'Success', 
      'You have successfully registered.',
      [
        { text: "Cancel", style: "cancel" },
        { text: "OK", onPress: () => navigation.goBack() }
      ]
    )
  }

  return (
    <View style={styles.viewRoot}>
      <View style={styles.viewBody}>
        <Image source={IMG_LOGO} style={styles.imgLogo} resizeMode='contain'/>

        <View style={styles.viewInput}>
          <TextInput
            style={styles.inputUsername}
            placeholder='Username' 
            placeholderTextColor={COLOR.GREY_999_HEX}
            autoCorrect={false}
            autoCapitalize={'none'}
            onChangeText={(text)=>setUsername(text)}
            value={username}
          />
          <View style={styles.viewPassword}>
            <TextInput
              style={styles.inputPassword}
              placeholder='Password' 
              placeholderTextColor={COLOR.GREY_999_HEX}
              autoCorrect={false}
              secureTextEntry={!isPasswordVisible}
              onChangeText={(text)=>setPassword(text)}
              value={password}
            />
            <TouchableOpacity 
              style={styles.btnEye}
              onPress={()=>setPasswordVisible(!isPasswordVisible)}
            >
              <Icon name={isPasswordVisible?'eye-off':'eye'} size={20} color={COLOR.GREY_999_HEX}/>
            </TouchableOpacity>
          </View>
          <View style={styles.viewPassword}>
            <TextInput
              style={styles.inputPassword}
              placeholder='Confirm Password' 
              placeholderTextColor={COLOR.GREY_999_HEX}
              autoCorrect={false}
              secureTextEntry={!isConfirmPasswordVisible}
              onChangeText={(text)=>setConfirmPassword(text)}
              value={confirmPassword}
            />
            <TouchableOpacity 
              style={styles.btnEye}
              onPress={()=>setConfirmPasswordVisible(!isConfirmPasswordVisible)}
            >
              <Icon name={isConfirmPasswordVisible?'eye-off':'eye'} size={20} color={COLOR.GREY_999_HEX}/>
            </TouchableOpacity>
          </View>
        </View>


        <TouchableOpacity style={styles.btnRegister} onPress={onPressRegister} disabled={isLoading}>
          {isLoading?<ActivityIndicator color={COLOR.WHITE_HEX}/>:<Text style={styles.btnRegisterText}>Register</Text>}
        </TouchableOpacity>

        <Text style={styles.btnReturnText} onPress={onPressReturn}>
          <Text>Already have account? </Text>
          <Text style={styles.btnReturnText2}>Login now</Text>
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  viewRoot:{
    flex:1, 
    backgroundColor:COLOR.WHITE_HEX, 
    padding:10, 
    // justifyContent:'center',
  },
  viewBody:{
    marginTop:'25%',
    width:'100%', 
    alignItems:'center'
  },
  viewInput:{
    width:'85%', 
    alignItems:'center'
  },
  viewPassword:{
    backgroundColor:COLOR.GREY_EEE_HEX, 
    borderRadius:15, 
    width:'100%', 
    flexDirection:'row',
    marginBottom:15,
  },
  imgLogo:{
    height:120, 
    marginBottom:50
  },
  inputUsername:{
    backgroundColor:COLOR.GREY_EEE_HEX, 
    borderRadius:15, 
    width:'100%', 
    paddingHorizontal:15, 
    height:50, 
    fontSize:17, 
    marginBottom:15,
    color: COLOR.BLACK_HEX
  },
  inputPassword:{
    paddingHorizontal:15, 
    height:50, 
    fontSize:17, 
    width:'85%',
    color: COLOR.BLACK_HEX
  },
  btnEye:{
    width:'15%', 
    alignItems:'center', 
    justifyContent:'center'
  },
  btnRegister:{
    backgroundColor:COLOR.THEME_HEX, 
    width:'85%', 
    padding:15, 
    borderRadius:15, 
    alignItems:'center', 
    marginTop:30
  },
  btnRegisterText:{
    fontSize:17,
    color:COLOR.WHITE_HEX,
    fontWeight:'bold'
  },
  btnPasswordText:{
    fontWeight:'bold', 
    alignSelf:'flex-end', 
    color:'transparent',
    // marginTop:15
  },
  btnReturnText:{
    marginTop:15, 
    color:COLOR.GREY_888_HEX
  },
  btnReturnText2:{
    fontWeight:'bold',
    color:COLOR.GREY_777_HEX
  },
});

export default Register;
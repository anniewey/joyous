import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import * as COLOR from '../colour';

const Profile = ({ navigation }) => {
  const [isLoading, setLoading] = useState(false)
  const [userName, setUserName] = useState('')

  const userData = useSelector(state=> state.currentUser)
  const dispatch = useDispatch()

  useEffect(async () => {
    if(userData){
      setUserName(userData.username)
    }
  }, [])


  const onPressLogout = () => {
    dispatch({
      type: 'userUpdated',
      payload: {
        data: null
      }
    })
  }

  return (
    <View style={styles.viewRoot}>
      <Text style={styles.textTitle}>Profile</Text>
      <View style={styles.viewBody}>
        <View style={styles.viewItem}> 
          <Text style={styles.textLabel}>Username</Text>
          <Text style={styles.textValue}>{userName}</Text>
        </View>
        <TouchableOpacity style={styles.btnLogout} onPress={onPressLogout} disabled={isLoading}>
          {isLoading?<ActivityIndicator color={COLOR.WHITE_HEX}/>:<Text style={styles.btnLogoutText}>Logout</Text>}
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  viewRoot:{
    backgroundColor:COLOR.WHITE_HEX, 
    flex:1, 
    paddingHorizontal:25
  },
  viewBody:{
    justifyContent:'space-between', 
    flex:1, 
    marginBottom:30
  },
  viewItem:{
    backgroundColor:COLOR.GREY_EEE_HEX, 
    padding:15, 
    borderRadius:10, 
    marginBottom:15, 
  },
  textTitle:{
    fontWeight:'bold', 
    fontSize:24, 
    marginBottom:30
  },
  textLabel:{
    fontSize:16, 
    fontWeight:'bold', 
    marginBottom:5
  },
  textValue:{
    fontSize:15
  },
  btnLogout:{
    backgroundColor:COLOR.THEME_HEX, 
    padding:15, 
    borderRadius:15, 
    alignItems:'center', 
    marginTop:30,
  },
  btnLogoutText:{
    fontSize:17,
    color:COLOR.WHITE_HEX,
    fontWeight:'bold'
  }
});

export default Profile;
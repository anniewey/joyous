import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  ActivityIndicator,
  Alert,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import * as COLOR from '../colour';

const IMG_LOGO = require('../../assets/image/logo.png')

const Login = ({ navigation }) => {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [isPasswordVisible, setPasswordVisible] = useState(false)
  const [isLoading, setLoading] = useState(false)

  const dispatch = useDispatch()
  const userListData = useSelector(state=> state.userList)


  const onPressRegister = () => {
    navigation.navigate('Register')
  }

  const onPressLogin = () => {
    if(!username && !password) return

    setLoading(true)

    let isFound = userListData.find(item=> item.username===username && item.password===password)

    console.log(isFound, userListData);
    if(isFound){
      setLoading(false)
      dispatch({
        type: 'userUpdated',
        payload: {
          data: isFound
        }
      })
    }
    else{
      setLoading(false)
      Alert.alert("Error", "Invalid username or password.")
    }
  }

  return (
    <View style={styles.viewRoot}>
      <View style={styles.viewBody}>
        <Image source={IMG_LOGO} style={styles.imgLogo} resizeMode='contain'/>

        <View style={styles.viewInput}>
          <TextInput
            style={styles.inputUsername}
            placeholder='Username' 
            placeholderTextColor={COLOR.GREY_999_HEX}
            autoCorrect={false}
            autoCapitalize={'none'}
            onChangeText={(text)=>setUsername(text)}
            value={username}
          />
          <View style={styles.viewPassword}>
            <TextInput
              style={styles.inputPassword}
              placeholder='Password' 
              placeholderTextColor={COLOR.GREY_999_HEX}
              autoCorrect={false}
              secureTextEntry={!isPasswordVisible}
              onChangeText={(text)=>setPassword(text)}
              value={password}
            />
            <TouchableOpacity 
              style={styles.btnEye}
              onPress={()=>setPasswordVisible(!isPasswordVisible)}
            >
              <Icon name={isPasswordVisible?'eye-off':'eye'} size={20} color={COLOR.GREY_999_HEX}/>
            </TouchableOpacity>
          </View>
          <Text style={styles.btnPasswordText}>Forget password?</Text>

        </View>


        <TouchableOpacity style={styles.btnLogin} onPress={onPressLogin} disabled={isLoading}>
          {isLoading?<ActivityIndicator color={COLOR.WHITE_HEX}/>:<Text style={styles.btnLoginText}>Login</Text>}
        </TouchableOpacity>
        <Text style={styles.btnRegisterText} onPress={onPressRegister}>
          <Text>Don't have account? </Text>
          <Text style={styles.btnRegisterText2}>Register now</Text>
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  viewRoot:{
    flex:1, 
    backgroundColor:COLOR.WHITE_HEX, 
    padding:10, 
    // justifyContent:'center',
  },
  viewBody:{
    marginTop:'30%',
    width:'100%', 
    alignItems:'center'
  },
  viewInput:{
    width:'85%', 
    alignItems:'center'
  },
  viewPassword:{
    backgroundColor:COLOR.GREY_EEE_HEX, 
    borderRadius:15, 
    width:'100%', 
    flexDirection:'row'
  },
  imgLogo:{
    height:120, 
    marginBottom:50
  },
  inputUsername:{
    backgroundColor:COLOR.GREY_EEE_HEX, 
    borderRadius:15, 
    width:'100%', 
    paddingHorizontal:15, 
    height:50, 
    fontSize:17, 
    marginBottom:15,
    color: COLOR.BLACK_HEX
  },
  inputPassword:{
    paddingHorizontal:15, 
    height:50, 
    fontSize:17, 
    width:'85%',
    color: COLOR.BLACK_HEX
  },
  btnEye:{
    width:'15%', 
    alignItems:'center', 
    justifyContent:'center'
  },
  btnLogin:{
    backgroundColor:COLOR.THEME_HEX, 
    width:'85%', 
    padding:15, 
    borderRadius:15, 
    alignItems:'center', 
    marginTop:30
  },
  btnLoginText:{
    fontSize:17,
    color:COLOR.WHITE_HEX,
    fontWeight:'bold'
  },
  btnPasswordText:{
    fontWeight:'bold', 
    alignSelf:'flex-end', 
    color:'transparent',
    // marginTop:15
  },
  btnRegisterText:{
    marginTop:15, 
    color:COLOR.GREY_888_HEX
  },
  btnRegisterText2:{
    fontWeight:'bold',
    color:COLOR.GREY_777_HEX
  },
  copyrightText:{
    fontSize:12, 
    marginVertical:10, 
    color:COLOR.GREY_999_HEX
  }
});

export default Login;
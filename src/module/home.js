import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  Alert,
  TouchableOpacity,
  View,
  TextInput
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import * as COLOR from '../colour';


const Home = ({ navigation }) => {
  const [userName, setUserName] = useState('')
  const [itemTitle, setItemTitle] = useState('')
  const [itemDesc, setItemDesc] = useState('')
  const [whiteboardItemVisible, setWhiteboardItemVisible] = useState(false)
  const [whiteboardItemEdit, setWhiteboardItemEdit] = useState(false)
  const [whiteboardItemAdd, setWhiteboardItemAdd] = useState(false)
  const [whiteboardItem, setWhiteboardItem] = useState(null)

  const dispatch = useDispatch()

  const userData = useSelector(state=> state.currentUser)
  const whiteboardData = useSelector(state=> state.whiteboardList.filter(item =>item.user === userData.username))


  useEffect(() => {
    if(userData) setUserName(userData.username)
  }, [])

  const onPressAdd=()=>{
    setWhiteboardItemVisible(true)

    setWhiteboardItemEdit(false)
    setWhiteboardItemAdd(true)

    setItemTitle('')
    setItemDesc('')
  }

  const onPressEdit=()=>{
    if(whiteboardItemAdd){
      // cancel add
      setWhiteboardItemAdd(false)
      setWhiteboardItemVisible(false)

      setItemTitle('')
      setItemDesc('')
    }
    else{
      // edit / cancel edit
      if(!whiteboardItemEdit){
        setItemTitle(whiteboardItem.title)
        setItemDesc(whiteboardItem.description)
      }

      setWhiteboardItemAdd(false)
      setWhiteboardItemEdit(!whiteboardItemEdit)
    }
  }

  const onPressReturn = () => {
    setWhiteboardItemVisible(false)
  }

  const onPressWhiteboard = (item) => () =>{
    setWhiteboardItemVisible(true)
    setWhiteboardItem(item)
  }

  const onPressAddItem=()=>{
    if(!itemTitle || !itemDesc){
      Alert.alert('Error', 'Please fill in all the empty fields.')
      return
    }

    dispatch({
      type: 'whiteboardAdded',
      payload: {
        data: { id: Date.now(), title: itemTitle, description: itemDesc, user:userName }
      }
    })

    Alert.alert('Success', 'Item has successfully added to your whiteboard.',)

    resetToggle()
  }

  const onPressUpdateItem = (id, title, description) => () => {
    dispatch({
      type: 'whiteboardUpdated',
      payload: {
        data: { id, title, description }
      }
    })

    Alert.alert('Success', 'Item has successfully updated in your whiteboard.',)

    resetToggle()
  }

  const onPressDeleteItem = (id) => () =>{
    Alert.alert(
      'Delete', 
      'Are you sure to delete this item?',
      [
        { text: "Cancel", style: "cancel" },
        { text: "OK", onPress: () => {
            dispatch({
              type: 'whiteboardDeleted',
              payload: { id }
            })
        
            Alert.alert('Success', 'Item has successfully deleted from your whiteboard.',)
        
            resetToggle()
          } 
        }
      ]
    )
  }

  const resetToggle = () => {
    setWhiteboardItemVisible(false)
    setWhiteboardItemEdit(false)
    setWhiteboardItemAdd(false)
    setWhiteboardItem(null)
    setItemTitle('')
    setItemDesc('')
  }

  const renderWhiteboard = ({item, index}) => {
    return(
      <TouchableOpacity onPress={onPressWhiteboard(item)}>
        <View style={{paddingBottom:15, borderBottomWidth:1, borderColor:COLOR.GREY_EEE_HEX, flexDirection:'row'}}>
          <Text style={{fontWeight:'bold', fontSize:20, color:COLOR.THEME_HEX, marginRight:10}}>{index + 1}</Text>
          <View>
            <Text style={{color:COLOR.GREY_444_HEX, fontSize:16, fontWeight:'600'}}>{item.title}</Text>
            <Text style={{color:COLOR.GREY_999_HEX}} numberOfLines={1}>{item.description}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  const renderWhiteboardItem = () => {
    return(    
      whiteboardItemEdit || whiteboardItemAdd?
        <View style={[styles.whiteboardList, {flex:1, justifyContent:'space-between'}]}>

          <View>
            <TextInput 
              autoFocus
              autoCorrect={false}
              placeholder='Title'
              placeholderTextColor={COLOR.GREY_999_HEX}
              style={{color:COLOR.GREY_444_HEX, fontSize:16, fontWeight:'600', marginBottom:10}}
              onChangeText={(text)=>setItemTitle(text)}
              value={itemTitle}
            />
            <TextInput 
              multiline
              autoCorrect={false}
              placeholder='Description'
              placeholderTextColor={COLOR.GREY_999_HEX}
              style={{color:COLOR.GREY_777_HEX, height:200, textAlignVertical: 'top'}}
              onChangeText={(text)=>setItemDesc(text)}
              value={itemDesc}
            />
          </View>
          
          {whiteboardItemAdd?
          <TouchableOpacity style={styles.btnReturn} onPress={onPressAddItem}>
            <Text style={styles.btnUpdateText}>Add to whiteboard</Text>
          </TouchableOpacity>
          :
          <View style={{flexDirection:'row'}}>
            <TouchableOpacity style={styles.btnUpdate} onPress={onPressUpdateItem(whiteboardItem.id, itemTitle, itemDesc)}>
              <Text style={styles.btnUpdateText}>Update</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btnDelete} onPress={onPressDeleteItem(whiteboardItem.id)}>
              <Text style={styles.btnDeleteText}>Delete</Text>
            </TouchableOpacity>
          </View>}
        </View>
      :
        <View style={[styles.whiteboardList, {flex:1, justifyContent:'space-between'}]}>
          <View>
            <Text style={{color:COLOR.GREY_444_HEX, fontSize:16, fontWeight:'600', marginBottom:10}}>{whiteboardItem.title}</Text>
            <Text style={{color:COLOR.GREY_777_HEX}}>{whiteboardItem.description}</Text>
          </View>

          <TouchableOpacity style={styles.btnReturn} onPress={onPressReturn}>
            <Text style={styles.btnUpdateText}>Return</Text>
          </TouchableOpacity>
        </View>
    )
  }

  return (
    <View style={styles.viewRoot}>
      <View style={styles.viewGreeting}>
        <View>
          <Text style={styles.textGreeting}>Hey there,</Text>
          <Text style={styles.textUserName}>{userName}</Text>
        </View>
        <TouchableOpacity disabled>
          <>
            <Icon name='island' size={40} style={styles.iconBell}/>
            {/* <View style={styles.viewAlert}/> */}
          </>
        </TouchableOpacity>
      </View>

      <TouchableOpacity disabled>
        <View style={styles.viewWhiteboard}>
          <Icon name='grease-pencil' style={{fontSize:35, color:COLOR.WHITE_HEX, marginRight:15}}/>
          <View style={{flex:1}}>
            <Text style={styles.textWhiteboard}>My Whiteboard</Text>
            <Text style={styles.textWhiteboardNote}>{`You have ${whiteboardData.length} ${whiteboardData.length > 1? 'items':'item'} in your whiteboard`}</Text>
          </View>
        </View>
      </TouchableOpacity>

      <TouchableOpacity onPress={whiteboardItemVisible?onPressEdit:onPressAdd}>
        <Text style={{fontWeight:'bold', color:COLOR.THEME_HEX, textAlign:'right', margin:5, marginBottom:15}}>
          {whiteboardItemVisible? whiteboardItemEdit||whiteboardItemAdd? 'Cancel' : 'Edit' : 'Add new item'}
        </Text>
      </TouchableOpacity>

      {whiteboardItemVisible? renderWhiteboardItem() :
        <FlatList 
          data={whiteboardData}
          renderItem={renderWhiteboard}
          style={styles.whiteboardList}
          ItemSeparatorComponent={()=>(<View style={styles.viewSeparator}/>)}
          keyExtractor={(item, index) => index.toString()}
        />
      }
    </View>
  );
}

const styles = StyleSheet.create({
  viewRoot:{
    flex:1,
    backgroundColor:COLOR.WHITE_HEX, 
    paddingHorizontal:25
  },
  viewGreeting:{
    flexDirection:'row', 
    alignItems:'center', 
    justifyContent:'space-between', 
    marginBottom:30
  },
  viewAlert:{
    width:10, 
    height:10, 
    backgroundColor:COLOR.RED_HEX, 
    position:'absolute', 
    top:2, 
    right:5, 
    borderRadius:5
  },
  viewWhiteboard:{
    padding:20, 
    backgroundColor:COLOR.THEME_HEX, 
    borderRadius:15, 
    marginBottom:10, 
    flexDirection:'row', 
    justifyContent:'space-between', 
    alignItems:'center'
  },
  viewSeparator:{
    height:20
  },
  textGreeting:{
    fontSize:18, 
    color:COLOR.GREY_999_HEX
  },
  textUserName:{
    fontWeight:'bold', 
    fontSize:24, 
    marginTop:5
  },
  textWhiteboard:{
    fontWeight:'bold', 
    fontSize:18, 
    color:COLOR.WHITE_HEX
  },
  textWhiteboardNote:{
    marginTop:10,
    color:COLOR.WHITE_HEX
  },
  iconBell:{
    color:COLOR.GREY_CCC_HEX
  },
  whiteboardList:{
    marginBottom:15,
    paddingHorizontal:10,
  },
  btnUpdate:{
    backgroundColor:COLOR.THEME_HEX, 
    padding:10, 
    borderRadius:15, 
    alignItems:'center',
    flex:1, 
    marginRight:10
  },
  btnUpdateText:{
    color:COLOR.WHITE_HEX,
    fontWeight:'bold'
  },
  btnDelete:{
    backgroundColor:COLOR.RED_HEX, 
    padding:10, 
    borderRadius:15, 
    alignItems:'center',
    flex:1
  },
  btnDeleteText:{
    color:COLOR.WHITE_HEX,
    fontWeight:'bold'
  },
  btnReturn:{
    backgroundColor:COLOR.THEME_HEX, 
    padding:10, 
    borderRadius:15, 
    alignItems:'center',
    marginRight:10
  },
  btnReturnText:{
    color:COLOR.WHITE_HEX,
    fontWeight:'bold'
  },
});

export default Home;